External Libraries used:-

Volley library for Networking call and (image downloading & caching) (https://github.com/google/volley)
Using the support Design Library (https://developer.android.com/training/material/design-library.html)
CardView & RecycleView (https://developer.android.com/training/material/lists-cards.html)
ButterKnife for Binding Andeoid Views (https://github.com/JakeWharton/butterknife)
Volley (HTTP library that makes networking call)
Gson Json Parser library (https://github.com/google/gson)
Circular Images (https://github.com/lopspower/CircularImageView)
Parcels making object parcebles (https://github.com/johncarl81/parceler)