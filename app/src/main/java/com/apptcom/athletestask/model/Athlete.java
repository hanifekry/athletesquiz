package com.apptcom.athletestask.model;


import org.parceler.Parcel;

@Parcel
public class Athlete {
    private String name, image,brief;

    public Athlete() {
    }

    public Athlete(String name, String image, String brief) {
        this.name = name;
        this.image = image;
        this.brief = brief;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getBrief() {
        return brief;
    }

}
