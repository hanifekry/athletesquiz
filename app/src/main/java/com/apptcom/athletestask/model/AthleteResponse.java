package com.apptcom.athletestask.model;

import java.util.List;

public class AthleteResponse {
    private List<Athlete> athletes;

    public AthleteResponse(List<Athlete> athletes){
        this.athletes = athletes;
    }

    public List<Athlete> getAthletes() {
        return athletes;
    }
}
