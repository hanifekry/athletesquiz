package com.apptcom.athletestask.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.apptcom.athletestask.R;
import com.apptcom.athletestask.app.AppController;
import com.apptcom.athletestask.model.Athlete;
import com.mikhaellopez.circularimageview.CircularImageView;
import org.parceler.Parcels;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AthleteSingleItemActivity extends AppCompatActivity{

    @BindView(R.id.athlete_photo) CircularImageView athletePhoto;
    @BindView(R.id.athlete_name) TextView athleteName;
    @BindView(R.id.athlete_brief) TextView athleteBrief;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.athlete_list_item);
        ButterKnife.bind(this);
        athleteBrief.setVisibility(View.VISIBLE);
        // make brief text scrollable
        athleteBrief.setMovementMethod(new ScrollingMovementMethod());
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        if (getIntent() != null) {
         Athlete athlete =  Parcels.unwrap(getIntent().getParcelableExtra("athlete"));
            imageLoader.get(athlete.getImage(), ImageLoader.getImageListener(
                    athletePhoto, R.drawable.placeholder_photo, R.drawable.erro_image));
            athleteName.setText(athlete.getName());
            athleteBrief.setText(athlete.getBrief());
        }
    }
}
