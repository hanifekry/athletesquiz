package com.apptcom.athletestask.views;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.apptcom.athletestask.R;
import com.apptcom.athletestask.app.AppController;
import com.apptcom.athletestask.model.Athlete;
import com.apptcom.athletestask.model.AthleteResponse;
import com.apptcom.athletestask.views.recycleView.AthleteAdapter;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    // Athlete json URL
    private static final String URL = "https://gist.githubusercontent.com/Bassem-Samy/f227855df4d197d3737c304e9377c4d4/raw/ece2a30b16a77ee58091886bf6d3445946e10a23/athletes.josn";
    private static final String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog dialog;
    private List<Athlete> athleteList = new ArrayList<>();
    @BindView(R.id.recycleView) RecyclerView recyclerView;
    private AthleteAdapter adapter;
    private LinearLayoutManager horizontalLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initializeViews();
        jsonObjectRequest();
    }

    private void initializeViews() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        horizontalLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
    }

    // Creating volley object request
    private void jsonObjectRequest(){
        final Gson gson = new Gson();
        JsonObjectRequest athleteRequest = new JsonObjectRequest(URL,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            Log.d(TAG, response.toString());
                            hidePDialog();
                            // Parsing json
                            AthleteResponse athleteResponse = gson.fromJson(response.toString(), AthleteResponse.class);
                            athleteList = athleteResponse.getAthletes();
                            adapter = new AthleteAdapter(MainActivity.this, athleteList);
                            recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                            recyclerView.setLayoutManager(horizontalLayoutManager);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }
                },
            new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(athleteRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

}
