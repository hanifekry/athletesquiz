package com.apptcom.athletestask.views.recycleView;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.apptcom.athletestask.R;
import com.apptcom.athletestask.app.AppController;
import com.apptcom.athletestask.model.Athlete;
import com.apptcom.athletestask.views.AthleteSingleItemActivity;

import org.parceler.Parcels;
import java.util.Collections;
import java.util.List;

public class AthleteAdapter extends RecyclerView.Adapter<AthleteHolder> {

    private List<Athlete> athleteList = Collections.emptyList();
    private Context context;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public AthleteAdapter(Context context , List<Athlete> athleteList) {
        this.athleteList = athleteList;
        this.context = context;
    }


    @Override
    public AthleteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.athlete_list_item, parent, false);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        return new AthleteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AthleteHolder holder, final int position) {
        imageLoader.get(athleteList.get(position).getImage(), ImageLoader.getImageListener(
                holder.athletePhoto, R.drawable.placeholder_photo, R.drawable.erro_image));
        holder.athleteName.setText(athleteList.get(position).getName());
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.athleteName.getLayoutParams();
        lp.addRule(RelativeLayout.CENTER_VERTICAL);
        holder.athleteName.setLayoutParams(lp);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get athlete object
                Athlete athlete = athleteList.get(position);
                Intent intent = new Intent(context, AthleteSingleItemActivity.class);
                // make athlete object parcelable (buy using Parcels library )
                // send athlete object to AthleteSingleItemActivity activity within intent
                intent.putExtra("athlete", Parcels.wrap(athlete));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount()
    {
        return athleteList.size();
    }
}