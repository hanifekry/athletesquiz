package com.apptcom.athletestask.views.recycleView;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.apptcom.athletestask.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import butterknife.BindView;
import butterknife.ButterKnife;

class AthleteHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.cardView) CardView cardView;
    @BindView(R.id.athlete_photo) CircularImageView  athletePhoto;
    @BindView(R.id.athlete_name) TextView athleteName;

    AthleteHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}